#!/usr/bin/python3

from sys import argv, exit
from subprocess import Popen, PIPE
from xml.etree import ElementTree

source = argv[1]

# Find source audio format using mediainfo as XML
MEDIAINFO_ARGS = ['mediainfo', '--Output=XML', source]

p = Popen(MEDIAINFO_ARGS, stdout=PIPE)
xmlOutput = p.stdout
p.wait()

tree = ElementTree.parse(xmlOutput)
for child in tree.iter():
	if child.tag == 'track' and child.attrib['type'] == 'Audio':
		if child.find('Default').text == 'Yes':
			audioFormat = child.find('Format').text
			break
		else:
			audioFormat = child.find('Format').text

# Transcode DTS audio format else copy with ffmpeg
FFMPEG_TRANSCODE_DTS_ARGS = ['ffmpeg', '-i', argv[1], '-map_metadata', '-1', '-c:v', 'copy', '-c:a', 'ac3', '-ab', '640k', argv[1].replace('.mkv', '.mp4')]
FFMPEG_COPY_ARGS = ['ffmpeg', '-i', argv[1], '-map_metadata', '-1', '-c:v', 'copy', '-c:a', 'copy', argv[1].replace('.mkv', '.mp4')]

if audioFormat == 'DTS':
	print('Transcoding DTS to AC3')
	code = Popen(FFMPEG_TRANSCODE_DTS_ARGS).wait()
elif audioFormat in ('AC-3', 'AAC'):
	print('Copying both stream to MP4')
	code = Popen(FFMPEG_COPY_ARGS).wait()
else:
	print('Unsuported audio format!')
	exit()

print('Programe as terminated with exit code {0}'.format(code))
exit()

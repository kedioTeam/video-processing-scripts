#!/usr/bin/python3

import os, subprocess

files = [f for f in os.listdir('.') if os.path.isfile(f)]
for f in files:
	outFile = f.replace('.avi', '.mp4')
	command = 'ffmpeg -i ' + f + ' -c:v copy -c:a libfdk_aac -vbr 3 ' + outFile
	subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, preexec_fn = os.setsid)


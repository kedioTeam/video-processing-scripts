#!/usr/bin/python3

import os, subprocess
import argparse

def rename_all_files(folder, title, count, decimals, fansub, filetype, season):
    files = [f for f in os.listdir(folder) if os.path.isfile(folder + f)]
    files.sort()
    for f in files:
        new_file_name = str.format('{} - {}{:0={}}{}.{}', title, season, count, decimals, fansub, filetype)
        os.rename(folder + f, folder + new_file_name)
        count += 1

def add_bracket(fansub):
    if fansub:
        fansub = str.format('[{}]', fansub)
    return fansub

def make_season_string(season_number):
    if season_number:
        season = str.format('s{:0=2}e', season_number)
    else:
        season = ''
    return season

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Rename all file in a folder')
    parser.add_argument('folder', type=str, help='Folder containing file to rename')
    parser.add_argument('title', type=str, help='Recurent name of the serie')
    parser.add_argument('-c', '--count', type=int, default=1, help='Stating number of the first episode to rename. Default to 1')
    parser.add_argument('-d', '--decimals', type=int, default=2, help='Number of decimals needed for the count. Default to 2')
    parser.add_argument('-f', '--fansub', type=str, default='', help='Name of the fansub')
    parser.add_argument('-t', '--filetype', type=str, default='mp4', help='File type extention. Default to mp4')
    parser.add_argument('-s', '--season', type=int, help='The season number if needed')

    args = parser.parse_args()
    fansub = add_bracket(args.fansub)
    season = make_season_string(args.season)
    rename_all_files(args.folder, args.title, args.count, args.decimals, fansub, args.filetype, season)

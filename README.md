# videoProcessingScripts
Some scripts I use to transform videos to .mp4 and make them playable on PS3 and iOS.

## Dependencies
**mkv2mp4.py and avi2mp4.py needs ffmpeg**

- OSX
  - `brew install ffmpeg --with-fdk-aac --with-libass`
- ubuntu
  - see http://doc.ubuntu-fr.org/ffmpeg

**keepSubtitles-mkv2mp4.ppy needs Handbrake-CLI**

- OSX
  - `brew cask install handbrakecli`
- ubuntu
  - `sudo add-apt-repository ppa:stebbins/handbrake-releases`
  - `sudo apt-get update`
  - `sudo apt-get install handbrake-cli`

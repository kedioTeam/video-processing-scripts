#!/usr/bin/python3

from argparse import ArgumentParser
from subprocess import call

if __name__ == '__main__':
    
    script_description = 'This script takes an ".mkv" file and produce\n' \
                         'an hard subbed ".mp4" file using HandBrakeCLI'       
    argument_parser = ArgumentParser(description=script_description)
    argument_parser.add_argument('mkvFile')
    argument_parser.add_argument('--subtitleID', '-s',  default='1')
    argument_parser.add_argument('--audioID', '-a', default='1')
    argument_parser.add_argument('--force720p', '-f', action="store_true")

    arguments = argument_parser.parse_args()
    
    # Build HandBrakeCLI command
    handbrake_binary = ['HandBrakeCLI']
    source_file = ['-i', arguments.mkvFile]
    destination_file = ['-f', 'mp4', '-4', '-o', arguments.mkvFile.replace('.mkv','.mp4')]
    video_codec = ['-e', 'x264']
    quality = ['-q', '18']
    x264_args = ['-x', 'ref=6:bframes=5:b-adapt=2:direct=auto:me=umh:subq=8:rc-lookahead=50:deblock=1,1']
    audio_codec = ['-a', arguments.audioID, '-E', 'copy', '--audio-copy-mask', 'aac,ac3', '--audio-fallback', 'ffac3']
    subtitles = ['-s', arguments.subtitleID, '--subtitle-burn', '--srt-codeset', 'utf8']
    if arguments.force720p:
        crop = ['--width', '1280', '--height', '720']
    else:
        crop = []
    
    handbrake_command = handbrake_binary + \
                        source_file + \
                        destination_file + \
                        video_codec + \
                        quality + \
                        x264_args + \
                        audio_codec + \
                        subtitles + \
                        crop

    print(handbrake_command)
    # Execute command
    call(handbrake_command)
